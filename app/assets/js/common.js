$(document).ready(function () {
    $('.modal[auto-replace]').on('show.bs.modal', function (event) {
        let data = $(event.relatedTarget).data();

        for(let key in data) {
            if(key.indexOf('replace') !== 0) continue;

            let value = data[key];
            let element = $(this).find('[data-' + key.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase() + ']');
            let options = element.data(key);

            if(options instanceof Object) {
                for(let func in options) {
                    let arg = options[func];

                    eval("element." + func + "(arg, value)");
                }
            } else {
                eval("element." + options + "(value)");
            }
        }

    });
});
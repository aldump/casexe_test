<?php

namespace App\Doctrine\Repository;

use App\Doctrine\Entity\User;

class UserRepository extends AbstractRepository
{
    protected $entityClassName = User::class;
}

<?php
/**
 * Created by PhpStorm.
 * User: dump
 * Date: 9/28/16
 * Time: 3:33 PM
 */

namespace App\Doctrine\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

abstract class AbstractRepository extends ServiceEntityRepository {

    protected $entityClassName = null;

    public function __construct(RegistryInterface $registry)
    {
        if(! isset($this->entityClassName)) {
            throw new ORMException('Set entity class name first');
        }

        parent::__construct($registry, $this->entityClassName);
    }

    public function removeByIds(array $ids) {
        $tableName = $this->getEntityManager()
            ->getClassMetadata($this->getEntityName())
            ->getTableName();

        $query = "DELETE FROM ".$tableName." WHERE id in (".implode(", ", $ids).");";

        $this->getEntityManager()
            ->getConnection()->prepare($query)
            ->execute();
    }

    public function save(object $entity) {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush($entity);
    }

    public function getList()
    {
        return $this->getListQuery()->getResult();
    }

    public function getListQuery()
    {
        $query = $this->createQueryBuilder('entity');

        return $query->getQuery();
    }

    public function getCount()
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('COUNT(entity)')
            ->from($this->getEntityName(), 'entity');

        return (int) $query->getQuery()->getSingleScalarResult();
    }

    public function update(object $entity)
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush($entity);
    }

    public function delete(object $entity)
    {
        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush($entity);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: dump
 * Date: 10/01/19
 * Time: 22:09
 */

namespace App\Doctrine\Repository\User;

use App\Doctrine\Entity\User;
use App\Doctrine\Entity\User\UserPrize;
use App\Doctrine\Repository\AbstractRepository;
use App\Prize\PrizeType;

class UserPrizeRepository extends AbstractRepository
{
    protected $entityClassName = UserPrize::class;

    public function refuse(UserPrize $userPrize) {
        $userPrize->setStatus(UserPrize::STATUS_CANCELED);
        $userPrize->setClaimedAt(new \DateTime());
        $this->update($userPrize);
    }

    public function getUserList(User $user) {
        return $this->createQueryBuilder('userPrize')
            ->where('userPrize.user = :user')
            ->setParameter('user', $user)
            ->getQuery();
    }

    public function getNotClaimedPrices($limit = 100) {
        return $this->createQueryBuilder('userPrize')
            ->andWhere('userPrize.status = :status')
            ->andWhere('userPrize.type in (:types)')
            ->setParameter('status', UserPrize::STATUS_NEW)
            ->setParameter('types', [PrizeType::MONEY, PrizeType::BONUS])
            ->getQuery()->getResult();
    }
}
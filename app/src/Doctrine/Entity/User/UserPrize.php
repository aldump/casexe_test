<?php
/**
 * Created by PhpStorm.
 * User: dump
 * Date: 10/01/19
 * Time: 22:08
 */

namespace App\Doctrine\Entity\User;

use App\Doctrine\Entity\User;
use App\Prize\PrizeType;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Doctrine\Repository\User\UserPrizeRepository")
 */
class UserPrize
{
    public const STATUS_NEW = 0;
    public const STATUS_CLAIMED = 1;
    public const STATUS_CANCELED = 2;

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Doctrine\Entity\User", inversedBy="prizes")
     */
    private $user;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $claimedAt;

    /**
     * @var int
     * @ORM\Column(type="smallint", options={"default"=0})
     */
    private $status = 0;

    /**
     * @var int
     * @ORM\Column(type="smallint")
     */
    private $type;

    /**
     * @var array
     * @ORM\Column(type="json_array")
     */
    private $options;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime|null
     */
    public function getClaimedAt(): ?\DateTime
    {
        return $this->claimedAt;
    }

    /**
     * @param \DateTime $claimedAt
     */
    public function setClaimedAt(\DateTime $claimedAt): void
    {
        $this->claimedAt = $claimedAt;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    public function getHumanStatus(): string {
        $types = [
            self::STATUS_NEW => 'New',
            self::STATUS_CLAIMED => 'Claimed',
            self::STATUS_CANCELED => 'Refused'
        ];

        return $types[$this->getType()] ?? '';
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    public function getHumanType(): string {
        $types = [
            PrizeType::MONEY => 'Money',
            PrizeType::BONUS => 'Bonus',
            PrizeType::ITEM => 'Item'
        ];

        return $types[$this->getType()] ?? '';
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param array $options
     */
    public function setOptions(array $options): void
    {
        $this->options = $options;
    }
}
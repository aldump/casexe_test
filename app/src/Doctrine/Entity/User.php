<?php

namespace App\Doctrine\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as FosUser;

/**
 * @ORM\Entity(repositoryClass="App\Doctrine\Repository\UserRepository")
 */
class User extends FosUser
{
    const ROLE_USER = 'ROLE_USER';
    const ROLE_ADMIN = 'ROLE_ADMIN';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Doctrine\Entity\User\UserPrize", mappedBy="user", fetch="EXTRA_LAZY",cascade={"persist"})
     */
    private $prizes;
}

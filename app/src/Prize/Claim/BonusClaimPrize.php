<?php
/**
 * Created by PhpStorm.
 * User: dump
 * Date: 11/01/19
 * Time: 00:55
 */

namespace App\Prize\Claim;

use App\Doctrine\Entity\User;
use App\Prize\PrizeType;
use App\Prize\Type\BonusPrize;
use App\Prize\Type\PrizeTypeInterface;
use App\Service\User\UserBonus;

class BonusClaimPrize implements ClaimPrizeInterface {

    /**
     * @var UserBonus
     */
    private $userBonus;

    /**
     * BonusClaimPrize constructor.
     * @param UserBonus $userBonus
     */
    public function __construct(UserBonus $userBonus)
    {
        $this->userBonus = $userBonus;
    }

    /**
     * @param User $user
     * @param PrizeTypeInterface $prize
     * @return bool
     */
    public function claim(User $user, PrizeTypeInterface $prize): bool
    {
        /** @var BonusPrize $prize */
        $this->userBonus->addBonus($user, $prize->getAmount());
        return true;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return PrizeType::BONUS;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: dump
 * Date: 10/01/19
 * Time: 21:17
 */

namespace App\Prize\Claim;

class ClaimPrizePool
{
    private $claimers = [];

    /**
     * @param ClaimPrizeInterface $prizeClaim
     */
    public function addClaimer(ClaimPrizeInterface $prizeClaim): void {
        $this->claimers[$prizeClaim->getType()] = $prizeClaim;
    }

    /**
     * @param int $type
     * @return ClaimPrizeInterface|null
     */
    public function getClaimerByType(int $type): ?ClaimPrizeInterface {
        return $this->claimers[$type] ?? null;
    }
}
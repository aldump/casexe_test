<?php
/**
 * Created by PhpStorm.
 * User: dump
 * Date: 11/01/19
 * Time: 00:27
 */

namespace App\Prize\Claim;

use App\Doctrine\Entity\User;
use App\Prize\Type\PrizeTypeInterface;

interface ClaimPrizeInterface {
    public function claim(User $user, PrizeTypeInterface $prize): bool;

    /**
     * @return int
     */
    public function getType(): int;
}
<?php
/**
 * Created by PhpStorm.
 * User: dump
 * Date: 11/01/19
 * Time: 00:55
 */

namespace App\Prize\Claim;

use App\Doctrine\Entity\User;
use App\Prize\PrizeType;
use App\Prize\Type\PrizeTypeInterface;

class ItemClaimPrize implements ClaimPrizeInterface {

    public function claim(User $user, PrizeTypeInterface $prize): bool
    {
        //TODO: Receive shipping information
        return true;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return PrizeType::ITEM;
    }
}
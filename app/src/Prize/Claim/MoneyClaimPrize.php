<?php
/**
 * Created by PhpStorm.
 * User: dump
 * Date: 11/01/19
 * Time: 00:55
 */

namespace App\Prize\Claim;

use App\Doctrine\Entity\User;
use App\Prize\PrizeType;
use App\Prize\Type\MoneyPrize;
use App\Prize\Type\PrizeTypeInterface;
use App\Service\User\UserBonus;
use App\Service\Withdraw\Bank\StubBank;

class MoneyClaimPrize implements ClaimPrizeInterface {

    /**
     * @var UserBonus
     */
    private $userBonus;
    /**
     * @var StubBank
     */
    private $bank;

    /**
     * BonusClaimPrize constructor.
     * @param UserBonus $userBonus
     * @param StubBank $bank
     */
    public function __construct(UserBonus $userBonus, StubBank $bank)
    {
        $this->userBonus = $userBonus;
        $this->bank = $bank;
    }

    /**
     * @param User $user
     * @param PrizeTypeInterface $prize
     * @return bool
     */
    public function claim(User $user, PrizeTypeInterface $prize): bool
    {
        /** @var MoneyPrize $prize */

        if($prize->isClaimAsBonus()) {
            $this->userBonus->convertMoneyIntoBonus($user, $prize->getAmount());
        } else {
            $this->bank->sendMoneyToUserAccount($user, $prize->getAmount());
        }

        return true;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return PrizeType::MONEY;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: dump
 * Date: 10/01/19
 * Time: 21:22
 */

namespace App\Prize\Type;

use App\Prize\PrizeType;

class MoneyPrize implements PrizeTypeInterface
{
    /**
     * @var float
     */
    private $amount;

    /**
     * @var bool
     */
    private $claimAsBonus = false;

    /**
     * @return int
     */
    public function getType(): int
    {
        return PrizeType::MONEY;
    }

    /**
     * @param array $data
     */
    public function restore(array $data): void
    {
        $this->amount = $data['amount'] ?? 0;
        $this->claimAsBonus = $data['claim_as_bonus'] ?? false;
    }

    /**
     * @return array
     */
    public function store(): array
    {
        return [
            'amount' => $this->amount,
            'claim_as_bonus' => $this->claimAsBonus
        ];
    }

    public function randomize(): void
    {
        //TODO: add config in db, interface to receive different configs by name
        //TODO: replace with proper randomizer
        $this->amount = rand(1, 100000);
    }

    /**
     * @return bool
     */
    public function isClaimAsBonus(): bool
    {
        return $this->claimAsBonus;
    }

    /**
     * @param bool $claimAsBonus
     */
    public function setClaimAsBonus(bool $claimAsBonus): void
    {
        $this->claimAsBonus = $claimAsBonus;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }
}
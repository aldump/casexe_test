<?php
/**
 * Created by PhpStorm.
 * User: dump
 * Date: 10/01/19
 * Time: 20:54
 */

namespace App\Prize\Type;

use App\Prize\Claim\ClaimPrizeInterface;

interface PrizeTypeInterface {
    /**
     * @return int
     */
    public function getType(): int;

    /**
     * @param array $data
     */
    public function restore(array $data): void ;

    /**
     * @return array
     */
    public function store(): array;

    public function randomize(): void;
}
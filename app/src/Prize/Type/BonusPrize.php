<?php
/**
 * Created by PhpStorm.
 * User: dump
 * Date: 10/01/19
 * Time: 21:50
 */

namespace App\Prize\Type;

use App\Prize\PrizeType;

class BonusPrize implements PrizeTypeInterface
{
    /**
     * @var int
     */
    private $amount;

    /**
     * @return int
     */
    public function getType(): int
    {
        return PrizeType::BONUS;
    }

    /**
     * @param array $data
     */
    public function restore(array $data): void
    {
        $this->amount = $data['amount'] ?? 0;
    }

    /**
     * @return array
     */
    public function store(): array
    {
        return [
            'amount' => $this->amount
        ];
    }

    public function randomize(): void
    {
        //TODO: add config in db, interface to receive different configs by name
        //TODO: replace with proper randomizer
        $this->amount = rand(1, 100000);
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }
}
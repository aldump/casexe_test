<?php
/**
 * Created by PhpStorm.
 * User: dump
 * Date: 10/01/19
 * Time: 21:22
 */

namespace App\Prize\Type;

use App\Prize\PrizeType;

class ItemPrize implements PrizeTypeInterface
{
    //TODO: better way: create table in db with prizes and store db id
    private $items = [
        "Item 0",
        "Item 1",
        "Item 2",
        "Item 3",
        "Item 4",
        "Item 5",
        "Item 6",
        "Item 7",
        "Item 8",
        "Item 9",
    ]; 

    /**
     * @var string
     */
    private $item;

    /**
     * @return int
     */
    public function getType(): int
    {
        return PrizeType::ITEM;
    }

    /**
     * @param array $data
     */
    public function restore(array $data): void
    {
        $this->item = $data['item'] ?? null;
    }

    /**
     * @return array
     */
    public function store(): array
    {
        return [
            'item' => $this->item
        ];
    }

    public function randomize(): void
    {
        //TODO: use proper randomizer
        $this->item = $this->items[array_rand($this->items)];
    }
}
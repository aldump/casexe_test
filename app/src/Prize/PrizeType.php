<?php
/**
 * Created by PhpStorm.
 * User: dump
 * Date: 10/01/19
 * Time: 21:55
 */

namespace App\Prize;

final class PrizeType {
    public const MONEY = 1;
    public const BONUS = 2;
    public const ITEM = 3;

    /**
     * PrizeType constructor.
     * @throws \Exception
     */
    private function __construct(){
        throw new \Exception("Can't get an instance of PrizeType");
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: dump
 * Date: 10/01/19
 * Time: 20:53
 */

namespace App\Prize;

use App\Doctrine\Entity\User;
use App\Prize\Claim\ClaimPrizePool;
use App\Prize\Claim\ClaimPrizeInterface;
use App\Prize\Type\PrizeTypeInterface;

class PrizeManager {
    /**
     * @var PrizePool
     */
    private $prizePool;
    /**
     * @var ClaimPrizePool
     */
    private $claimPrizePool;

    /**
     * PrizeManager constructor.
     * @param PrizePool $prizePool
     * @param ClaimPrizePool $claimPrizePool
     */
    public function __construct(PrizePool $prizePool, ClaimPrizePool $claimPrizePool)
    {
        //TODO: check if items and money available and remove from pool if not
        $this->prizePool = $prizePool;
        $this->claimPrizePool = $claimPrizePool;
    }

    public function getRandomPrize(): PrizeTypeInterface {
        $prize = $this->prizePool->getRandomPrize();
        $prize->randomize();

        return $prize;
    }

    public function claimPrize(User $user, int $type, $options = []): bool {
        $prize = $this->restorePrize($type, $options);
        $claimer = $this->claimPrizePool->getClaimerByType($prize->getType());

        if($claimer instanceof ClaimPrizeInterface) {
            return $claimer->claim($user, $prize);
        }

        return false;
    }

    public function restorePrize(int $type, array $options): PrizeTypeInterface {
        $prize = $this->prizePool->getPrizeByType($type);
        $prize->restore($options);

        return $prize;
    }
}
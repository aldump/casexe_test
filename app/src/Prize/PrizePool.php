<?php
/**
 * Created by PhpStorm.
 * User: dump
 * Date: 10/01/19
 * Time: 21:17
 */

namespace App\Prize;

use App\Prize\Type\PrizeTypeInterface;

class PrizePool
{
    private $prizes = [];

    /**
     * @param PrizeTypeInterface $prizeType
     */
    public function addPrize(PrizeTypeInterface $prizeType): void {
        $this->prizes[$prizeType->getType()] = $prizeType;
    }

    /**
     * @param int $type
     * @return PrizeTypeInterface|null
     */
    public function getPrizeByType(int $type): ?PrizeTypeInterface {
        return $this->prizes[$type] ?? null;
    }

    /**
     * @TODO: add randomizer
     * @return PrizeTypeInterface
     */
    public function getRandomPrize(): PrizeTypeInterface {
        return $this->prizes[array_rand($this->prizes)];
    }
}
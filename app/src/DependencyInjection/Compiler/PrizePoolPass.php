<?php
/**
 * Created by PhpStorm.
 * User: dump
 * Date: 4/19/17
 * Time: 5:07 PM
 */

namespace App\DependencyInjection\Compiler;

use App\Prize\PrizePool;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

/**
 * This compiler pass adds language providers tagged
 * with security.expression_language_provider to the
 * expression language used in the framework extra bundle.
 *
 * This allows to use custom expression language functions
 * in the @Security-Annotation.
 *
 * Symfony\Bundle\FrameworkBundle\DependencyInection\Compiler\AddExpressionLanguageProvidersPass
 * does the same, but only for the security.expression_language
 * which is used in the ExpressionVoter.
 */
class PrizePoolPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if ($container->has(PrizePool::class)) {
            $definition = $container->findDefinition(PrizePool::class);

            foreach ($container->findTaggedServiceIds('casexe.prize') as $id => $tags) {
                $definition->addMethodCall('addPrize', array(new Reference($id)));
            }
        }
    }
}
<?php


namespace App\Command;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Created by PhpStorm.

 * Date: 02.03.15
 * Time: 11:28
 */

class TestCommand extends Command {

    public function configure() {
        $this
            ->setName("ct:test")
            ->setDescription("Test");
    }

    public function execute(InputInterface $input, OutputInterface $output) {
        var_dump("test");
    }
}
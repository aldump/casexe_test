<?php


namespace App\Command;
use App\Doctrine\Entity\User\UserPrize;
use App\Doctrine\Repository\User\UserPrizeRepository;
use App\Prize\PrizeManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ClaimCommand extends Command {

    /**
     * @var UserPrizeRepository
     */
    private $userPrizeRepository;

    /**
     * @var PrizeManager
     */
    private $prizeManager;

    /**
     * ClaimCommand constructor.
     * @param UserPrizeRepository $userPrizeRepository
     * @param PrizeManager $prizeManager
     */
    public function __construct(UserPrizeRepository $userPrizeRepository, PrizeManager $prizeManager)
    {
        parent::__construct();
        $this->userPrizeRepository = $userPrizeRepository;
        $this->prizeManager = $prizeManager;
    }

    public function configure() {
        $this
            ->setName("ct:claim")
            ->setDescription("Claim prizes")
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output) {
        /** @var UserPrize $userPrize */
        foreach ($this->userPrizeRepository->getNotClaimedPrices() as $userPrize) {
            if($this->prizeManager->claimPrize($userPrize->getUser(), $userPrize->getType(), $userPrize->getOptions())) {
                $userPrize->setStatus(UserPrize::STATUS_CLAIMED);
                $userPrize->setClaimedAt(new \DateTime());
                $this->userPrizeRepository->save($userPrize);
            }
        }
    }
}
<?php

namespace App\Controller;

use App\Doctrine\Repository\AbstractRepository;
use Knp\Component\Pager\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class BaseController extends AbstractController
{
    /**
     * @var AbstractRepository
     */
    protected $repository;
    protected $paginationLimit = 50;

    /**
     * @return Paginator
     */
    protected function getPaginator()
    {
        return $this->get('knp_paginator');
    }


    protected function back($backUrl, $defaultUrl, $ignoreBackCheck = false) {

        if($backUrl && ! $ignoreBackCheck) {
            if(strpos($backUrl, $defaultUrl) === false) {
                return $this->redirect($defaultUrl);
            }
        }

        return $this->redirect($backUrl ?? $defaultUrl);
    }

    /**
     * @param $type
     * @param $model
     * @param Request $request
     * @param array $options
     * @return FormInterface
     */
    protected function prepareForm($type, $model, Request $request, $options = []) {
        $form = $this->createForm($type, $model, $options);
        $form->add('redirect_back_url', HiddenType::class, ['mapped' => false]);

        $form->handleRequest($request);

        if(empty($form->get('redirect_back_url')->getData()) && ! $form->isSubmitted()) {
            $form->get('redirect_back_url')->setData($request->headers->get('referer'));
        }

        return $form;
    }

    protected function getBackUrl(FormInterface $form) {
        return $form->has('redirect_back_url') ? $form->get('redirect_back_url')->getData() : false;
    }

    protected function simpleIndex(Request $request) {
        return array(
            'pagination' => $this->getPaginator()->paginate(
                $this->repository->getListQuery(),
                $request->query->get('page', 1),
                $this->paginationLimit
            )
        );
    }

    protected function simpleForm(Request $request, $entityClass, $type, $defaultRoute, $routeParams = array(), $saveMethod = 'save') {
        if(is_object($entityClass)) {
            $entity = $entityClass;
        } else {
            $entity = new $entityClass();
        }

        $form = $this->prepareForm($type, $entity, $request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $this->beforeSave($entity);

            $this->repository->$saveMethod($entity);

            $this->afterSave($entity);

            return $this->back($this->getBackUrl($form), $this->generateUrl($defaultRoute, $routeParams));
        }

        return array(
            'form' => $form->createView(),
            'entity' => $entity
        );
    }

    protected function simpleDelete(Request $request, $entity, $token, $defaultRoute, $routeParams = array()) {
        if ($this->isCsrfTokenValid($token, $request->get('token'))) {
            $this->repository->delete($entity);
        }

        return $this->back($request->headers->get('referer'), $this->generateUrl($defaultRoute, $routeParams));
    }

    protected function afterSave($entity) {

    }

    protected function beforeSave($entity) {

    }
}

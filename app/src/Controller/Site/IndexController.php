<?php

namespace App\Controller\Site;

use App\Controller\BaseController;
use App\Doctrine\Entity\User\UserPrize;
use App\Doctrine\Repository\User\UserPrizeRepository;
use App\Prize\PrizeManager;
use App\Prize\Type\MoneyPrize;
use App\Prize\Type\PrizeTypeInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Template
 * @Route("/")
 */
class IndexController extends BaseController {

    /**
     * @var UserPrizeRepository
     */
    protected $repository;

    /**
     * IndexController constructor.
     * @param UserPrizeRepository $repository
     */
    public function __construct(UserPrizeRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route(name="site-index")
     * @param Request $request
     * @return array
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        return array(
            'pagination' => $paginator->paginate(
                $this->repository->getUserList($this->getUser()),
                $request->query->get('page', 1),
                $this->paginationLimit
            )
        );
    }

    /**
     * @Route("/get-prize", name="site-index:get-prize")
     * @param PrizeManager $prizeManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function getPrize(PrizeManager $prizeManager) {
        /** @var PrizeTypeInterface $prize */
        $prize = $prizeManager->getRandomPrize();

        $userPrize = new UserPrize();
        $userPrize->setType($prize->getType());
        $userPrize->setUser($this->getUser());
        $userPrize->setOptions($prize->store());

        $this->repository->save($userPrize);

        return $this->redirect($this->generateUrl('site-index'));
    }

    /**
     * @Route("/refuse/{id}", requirements={"id" = "\d+"}, name="site-index:refuse", methods={"post"})
     * @ParamConverter("userPrize")
     * @param Request $request
     * @param UserPrize $userPrize
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function refuse(Request $request, UserPrize $userPrize)
    {
        if ($this->isCsrfTokenValid('prize_refuse', $request->get('token'))) {
            $this->repository->refuse($userPrize);
        }

        return $this->back($request->headers->get('referer'), $this->generateUrl('site-index'));
    }

    /**
     * @Route("/claim-as-bonus/{id}", requirements={"id" = "\d+"}, name="site-index:claim-as-bonus", methods={"post"})
     * @ParamConverter("userPrize")
     * @param Request $request
     * @param UserPrize $userPrize
     * @param PrizeManager $prizeManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function claimAsBonus(Request $request, UserPrize $userPrize, PrizeManager $prizeManager)
    {
        if ($this->isCsrfTokenValid('prize_refuse', $request->get('token'))) {

            /** @var MoneyPrize $prize */
            $prize = $prizeManager->restorePrize($userPrize->getType(), $userPrize->getOptions());

            if($prize instanceof MoneyPrize) {
                $prize->setClaimAsBonus(true);
                $userPrize->setOptions($prize->store());

                $this->repository->update($userPrize);
            } else {
                //error
            }


        }

        return $this->back($request->headers->get('referer'), $this->generateUrl('site-index'));
    }
}
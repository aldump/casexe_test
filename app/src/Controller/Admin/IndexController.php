<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Template
 * @Route("/")
 * @Security("has_role('ROLE_USER')")
 */
class IndexController extends BaseController {

    /**
     * @Route("/", name="admin-index")
     * @return array
     */
    public function indexAction()
    {

        return array(
        );
    }
}
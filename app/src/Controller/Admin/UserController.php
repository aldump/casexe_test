<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Doctrine\Entity\User;
use App\Doctrine\Repository\UserRepository;
use App\Form\Type\Admin\UserType;
use FOS\UserBundle\Model\UserManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Template
 * @Route("/user")
 * @Security("has_role('ROLE_ADMIN')")
 */
class UserController extends BaseController {

    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * @var UserManagerInterface
     */
    private $userManager;

    /**
     * UserController constructor.
     * @param UserRepository $repository
     * @param UserManagerInterface $userManager
     */
    public function __construct(UserRepository $repository, UserManagerInterface $userManager)
    {
        $this->repository = $repository;
        $this->userManager = $userManager;
    }

    /**
     * @Route("/", name="admin-user")
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        return $this->simpleIndex($request);
    }

    /**
     * @Route("/create", name="admin-user:create")
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function create(Request $request)
    {
        $user = $this->userManager->createUser();
        $user->setEnabled(true);

        $form = $this->prepareForm(UserType::class, $user, $request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $this->userManager->updateUser($user);

            return $this->back($this->getBackUrl($form), $this->generateUrl('admin-user'));
        }

        return array(
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/edit/{id}", requirements={"id" = "\d+"}, name="admin-user:update")
     * @ParamConverter("user")
     * @param Request $request
     * @param User $user
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function update(Request $request, User $user)
    {
        $form = $this->prepareForm(UserType::class, $user, $request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $this->userManager->updateUser($user);

            return $this->back($this->getBackUrl($form), $this->generateUrl('admin-user'));
        }

        return array(
            'form' => $form->createView(),
            'user' => $user,
        );
    }

    /**
     * @Route("/delete/{id}", requirements={"id" = "\d+"}, name="admin-user:delete", methods={"post"})
     * @ParamConverter("user")
     * @param Request $request
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Request $request, User $user)
    {
        return $this->simpleDelete($request, $user, 'user_delete',"admin-user");
    }
}